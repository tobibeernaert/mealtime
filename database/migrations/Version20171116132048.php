<?php

namespace Tobinho\Mealtime\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171116132048 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ingredients (id CHAR(36) NOT NULL COMMENT \'(DC2Type:IngredientId)\', recipe_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:RecipeId)\', name VARCHAR(255) NOT NULL COMMENT \'(DC2Type:IngredientName)\', amount_value DOUBLE PRECISION DEFAULT NULL, amount_unit VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:Unit)\', INDEX IDX_4B60114F59D8A214 (recipe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ingredients ADD CONSTRAINT FK_4B60114F59D8A214 FOREIGN KEY (recipe_id) REFERENCES recipes (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ingredients');
    }
}

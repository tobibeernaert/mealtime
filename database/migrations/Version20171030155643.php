<?php

namespace Tobinho\Mealtime\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171030155643 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE authors (id CHAR(36) NOT NULL COMMENT \'(DC2Type:AuthorId)\', name VARCHAR(255) NOT NULL COMMENT \'(DC2Type:AuthorName)\', website VARCHAR(255) DEFAULT \'\' COMMENT \'(DC2Type:StrictUrl)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meals (id CHAR(36) NOT NULL COMMENT \'(DC2Type:MealId)\', date DATE NOT NULL COMMENT \'(DC2Type:Date)\', type VARCHAR(32) DEFAULT \'\' NOT NULL COMMENT \'(DC2Type:MealType)\', name VARCHAR(255) DEFAULT \'\' NOT NULL COMMENT \'(DC2Type:MealName)\', recipe_url VARCHAR(255) DEFAULT \'\' COMMENT \'(DC2Type:RecipeUrl)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recipes (id CHAR(36) NOT NULL COMMENT \'(DC2Type:RecipeId)\', author_id CHAR(36) NOT NULL COMMENT \'(DC2Type:AuthorId)\', name VARCHAR(255) NOT NULL COMMENT \'(DC2Type:RecipeName)\', url VARCHAR(255) DEFAULT \'\' COMMENT \'(DC2Type:StrictUrl)\', INDEX IDX_A369E2B5F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE recipes ADD CONSTRAINT FK_A369E2B5F675F31B FOREIGN KEY (author_id) REFERENCES authors (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE recipes DROP FOREIGN KEY FK_A369E2B5F675F31B');
        $this->addSql('DROP TABLE authors');
        $this->addSql('DROP TABLE meals');
        $this->addSql('DROP TABLE recipes');
    }
}

<?php
/** Required to get the Doctrine Console tool to work */
require 'vendor/autoload.php';

use Tobinho\Mealtime\Application\Application;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

$app = new Application(__DIR__);
$entityManager = $app->getContainer()->get(EntityManager::class);
return ConsoleRunner::createHelperSet($entityManager);

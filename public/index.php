<?php

use Tobinho\Mealtime\Application\Application;

require __DIR__.'/../vendor/autoload.php';

$app = new Application(__DIR__.'/..');
$app->run();

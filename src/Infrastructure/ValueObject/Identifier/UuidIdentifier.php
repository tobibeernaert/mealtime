<?php

namespace Tobinho\Mealtime\Infrastructure\ValueObject\Identifier;

use Ramsey\Uuid\Uuid;

abstract class UuidIdentifier
{
    /**
     * @var string
     */
    private $id;

    public function __construct(string $id =null)
    {
        $this->id = null === $id ? Uuid::uuid4()->toString() : $id;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function equals(UuidIdentifier $uuid): bool
    {
        return $this->id() === $uuid->id();
    }

    public function __toString(): string
    {
        return $this->id();
    }
}

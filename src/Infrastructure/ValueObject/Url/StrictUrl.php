<?php

namespace Tobinho\Mealtime\Infrastructure\ValueObject\Url;

use Tobinho\Mealtime\Infrastructure\ValueObject\Url\Url;

class StrictUrl extends Url
{
    public function isValid(string $url): bool
    {
        if (false === strpos($url, 'http://') && false === strpos($url, 'https://')) {
            return false;
        }

        return parent::isValid($url);
    }
}

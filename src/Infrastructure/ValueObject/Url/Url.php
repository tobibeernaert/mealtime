<?php

namespace Tobinho\Mealtime\Infrastructure\ValueObject\Url;

use Tobinho\Mealtime\Infrastructure\ValueObject\String\StringLiteral;
use Respect\Validation\Rules\Url as UrlValidator;
use InvalidArgumentException;

class Url extends StringLiteral
{
    public function __construct(string $url)
    {
        if  (!$this->isValid($url)) {
            throw new InvalidArgumentException("Invalid URL: $url");
        }

        parent::__construct($url);
    }

    public function isValid(string $url): bool
    {
        return (new UrlValidator())->validate($url);
    }
}

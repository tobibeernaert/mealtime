<?php

namespace Tobinho\Mealtime\Infrastructure\ValueObject\String;

use Tobinho\Mealtime\Infrastructure\ValueObject\String\StringLiteral;
use InvalidArgumentException;

class NonEmptyStringLiteral extends StringLiteral
{
    /**
     * @throw InvalidArgumentException
     */
    public function __construct(string $value)
    {
        parent::__construct($value);

        if (!preg_match('/\S/', $value)) {
            throw new InvalidArgumentException("Instance of class " . __CLASS__ . " can not be empty.");
        }
    }
}

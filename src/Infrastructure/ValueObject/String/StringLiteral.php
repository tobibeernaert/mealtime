<?php

namespace Tobinho\Mealtime\Infrastructure\ValueObject\String;

use JsonSerializable;

class StringLiteral implements JsonSerializable
{
    /**
     * @var string
     */
    protected $value;

    public function __construct(string $value)
    {
        $this->value = (string)$value;
    }

    public static function fromString(string $value): StringLiteral
    {
        return new static($value);
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public static function map(array $values): array
    {
        $stringLiterals = [];
        foreach ($values as $value) {
            $stringLiterals[] = new static($value);
        }
        return $stringLiterals;
    }

    public static function deserialize(array $data): StringLiteral
    {
        return static::fromString($data['value']);
    }

    public function serialize(): array
    {
        return [
            'value' => (string) $this->value,
        ];
    }

    public function jsonSerialize(): string
    {
        return $this->__toString();
    }

    public function equals(StringLiteral $stringLiteral): bool
    {
        return $this->value === "".$stringLiteral;
    }
}

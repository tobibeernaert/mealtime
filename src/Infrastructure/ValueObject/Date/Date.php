<?php

namespace Tobinho\Mealtime\Infrastructure\ValueObject\Date;

use Cake\Chronos\Date as ChronosDate;
use DateTime;
use Cake\Chronos\ChronosInterface;

class Date
{
    /**
     * @var ChronosDate
     */
    private $date;

    public function __construct(ChronosDate $date)
    {
        $this->date = $date;
    }

    public function now(): Date
    {
        return new static(ChronosDate::now());
    }

    public static function fromDateTime(DateTime $date): Date
    {
        return new static(ChronosDate::instance($date));
    }

    public function format($format): string
    {
        return $this->date->format($format);
    }

    public function addDays(int $days = 1): Date
    {
        return new static($this->date->addDays($days));
    }

    public function addDay(): Date
    {
        return $this->addDays();
    }

    public function subDays(int $days = 1): Date
    {
        return new static($this->date->subDays($days));
    }

    public function subDay(): Date
    {
        return $this->subDays();
    }

    public function startOfWeek(): Date
    {
        return new static($this->date->startOfWeek());
    }

    public function startOfNextWeek(): Date
    {
        $date = $this->date->next(ChronosInterface::MONDAY)->startOfWeek();
        return new static($date);
    }

    public function startOfLastWeek(): Date
    {
        $date = $this->date->previous(ChronosInterface::SUNDAY)->startOfWeek();
        return new static($date);
    }

    public function endOfWeek(): Date
    {
        return new static($this->date->endOfWeek());
    }

    public function endOfNextWeek(): Date
    {
        $date = $this->date->next(ChronosInterface::MONDAY)->endOfWeek();
        return new static($date);
    }

    public function endOfLastWeek(): Date
    {
        $date = $this->date->previous(ChronosInterface::SUNDAY)->endOfWeek();
        return new static($date);
    }
}

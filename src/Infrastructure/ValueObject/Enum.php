<?php

namespace Tobinho\Mealtime\Infrastructure\ValueObject;

use ReflectionClass;
use InvalidArgumentException;

abstract class Enum
{
    /**
     * @var array
     */
    private $options;

    /**
     * @var string
     */
    private $value;

    public function __construct(string $value)
    {
        if (!$this->isConstantDefined($value)) {
            throw new InvalidArgumentException($value." is not a valid enum value for ".static::class);
        }
        $this->value = $value;
    }

    final public function getValue(): string
    {
        return $this->value;
    }

    final public function isConstantDefined(string $value): bool
    {
        $this->initEnumOptions();
        return in_array($value, $this->options, true);
    }

    final public function getName(): string
    {
        $this->initEnumOptions();
        return array_search($this->value, $this->options, true);
    }

    final public function initEnumOptions(): void
    {
        if (null === $this->options) {
            $this->options = [];
            $class = new ReflectionClass(static::class);
            foreach ($class->getReflectionConstants() as $constant) {
                // We only want to include publicly available options.
                if ($constant->isPublic()) {
                    $this->options[$constant->getName()] = $constant->getValue();
                }
            }
        }
    }

    public function __toString(): string
    {
        return $this->getValue();
    }
}

<?php
namespace Tobinho\Mealtime\Infrastructure\Doctrine\Listener;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

final class NullableEmbeddableListener
{
    private $propertyAccessor;
    private $propertyMap = [];

    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        $this->propertyAccessor = $propertyAccessor;
    }

    public function addMapping(string $entity, string $propertyPath)
    {
        if (empty($this->propertyMap[$entity])) {
            $this->propertyMap[$entity] = [];
        }

        $this->propertyMap[$entity][] = $propertyPath;
    }

    public function postLoad($object)
    {
        if (empty($this->propertyMap[get_class($object)])) {
            return;
        }

        $propertyPaths = $this->propertyMap[get_class($object)];
        foreach ($propertyPaths as $propertyPath) {
            $embeddable = $this->propertyAccessor->getValue($object, $propertyPath);

            if ($embeddable->isNull()) {
                $this->propertyAccessor->setValue($object, $propertyPath, null);
            }
        }
    }
}

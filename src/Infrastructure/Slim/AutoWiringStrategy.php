<?php

namespace Tobinho\Mealtime\Infrastructure\Slim;

use League\Container\Container;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\InvocationStrategyInterface;

class AutoWiringStrategy implements InvocationStrategyInterface
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function __invoke(
        callable $callable,
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $routeArguments
    ) {
        // Bind the latest request and response instance to the container.
        $this->container->add('request', $request);
        $this->container->add('response', $response);

        return $this->container->call($callable, $routeArguments);
    }
}

<?php

namespace Tobinho\Mealtime\Application\Recipe\Transformer;

use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use Tobinho\Mealtime\Domain\Recipe\Recipe;
use Tobinho\Mealtime\Application\Author\Transformer\AuthorTransformer;
use Tobinho\Mealtime\Application\Recipe\Ingredient\Transformer\IngredientTransformer;

class RecipeTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = [
        'author',
        'ingredients',
    ];

    /**
     * {@inheritDoc}
     */
    public function transform(Recipe $recipe): array
    {
        return [
            'id' => $recipe->getId()->id(),
            'name' => $recipe->getName(),
            'url' => $recipe->getUrl(),
        ];
    }

    public function includeAuthor(Recipe $recipe)
    {
        return $this->item($recipe->getAuthor(), new AuthorTransformer());
    }

    public function includeIngredients(Recipe $recipe): Collection
    {
        return $this->collection($recipe->getIngredients(), new IngredientTransformer());
    }
}

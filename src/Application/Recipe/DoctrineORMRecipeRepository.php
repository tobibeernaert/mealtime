<?php

namespace Tobinho\Mealtime\Application\Recipe;

use Doctrine\ORM\EntityRepository;
use Tobinho\Mealtime\Domain\Recipe\RecipeRepository;
use Tobinho\Mealtime\Domain\Recipe\Recipe;
use Tobinho\Mealtime\Domain\Recipe\RecipeId;

class DoctrineORMRecipeRepository extends EntityRepository implements RecipeRepository
{
    final public function save(Recipe $recipe)
    {
        $this->_em->persist($recipe);
        $this->_em->flush($recipe);
    }

    final public function delete(Recipe $recipe)
    {
        $this->_em->remove($recipe);
        $this->_em->flush($recipe);
    }

    final public function all(): array
    {
        return $this->findAll();
    }

    final public function one(RecipeId $recipeId):? Recipe
    {
        return $this->find($recipeId);
    }
}

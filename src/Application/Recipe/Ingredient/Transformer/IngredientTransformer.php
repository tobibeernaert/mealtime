<?php

namespace Tobinho\Mealtime\Application\Recipe\Ingredient\Transformer;

use League\Fractal\TransformerAbstract;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Ingredient;

class IngredientTransformer extends TransformerAbstract
{
    public function transform(Ingredient $ingredient): array
    {
        $ingredientArray = [
            'id' => $ingredient->getId()->id(),
            'name' => $ingredient->getIngredientName(),
        ];

        if ($ingredient->hasAmount()) {
            $ingredientArray['amount'] = $ingredient->getAmount()->toArray();
        }

        return $ingredientArray;
    }
}

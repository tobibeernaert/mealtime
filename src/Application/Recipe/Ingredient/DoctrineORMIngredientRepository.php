<?php

namespace Tobinho\Mealtime\Application\Recipe\Ingredient;

use Doctrine\ORM\EntityRepository;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientRepository;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Ingredient;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientId;

class DoctrineORMIngredientRepository extends EntityRepository implements IngredientRepository
{
    final public function save(Ingredient $ingredient)
    {
        $this->_em->persist($ingredient);
        $this->_em->flush($ingredient);
    }

    final public function delete(Ingredient $ingredient)
    {
        $this->_em->remove($ingredient);
        $this->_em->flush($ingredient);
    }

    final public function all(): array
    {
        return $this->findAll();
    }

    final public function one(IngredientId $ingredientId):? Ingredient
    {
        return $this->find($ingredientId);
    }
}

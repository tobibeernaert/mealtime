<?php

namespace Tobinho\Mealtime\Application\Author;

use Doctrine\ORM\EntityRepository;
use Tobinho\Mealtime\Domain\Author\AuthorRepository;
use Tobinho\Mealtime\Domain\Author\Author;
use Tobinho\Mealtime\Domain\Author\AuthorId;

class DoctrineORMAuthorRepository extends EntityRepository implements AuthorRepository
{
    final public function save(Author $author)
    {
        $this->_em->persist($author);
        $this->_em->flush($author);
    }

    final public function delete(Author $author)
    {
        $this->_em->remove($author);
        $this->_em->flush($author);
    }

    final public function all(): array
    {
        return $this->findAll();
    }

    final public function one(AuthorId $authorId):? Author
    {
        return $this->find($authorId);
    }
}

<?php

namespace Tobinho\Mealtime\Application\Author\Transformer;

use League\Fractal\TransformerAbstract;
use Tobinho\Mealtime\Domain\Author\Author;

class AuthorTransformer extends TransformerAbstract
{
    /**
     * {@inheritDoc}
     */
    public function transform(Author $author)
    {
        return [
            'id' => $author->getId()->id(),
            'name' => $author->getName(),
            'website' => $author->getWebsite(),
        ];
    }
}

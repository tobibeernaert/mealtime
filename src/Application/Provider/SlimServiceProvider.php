<?php

namespace Tobinho\Mealtime\Application\Provider;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Slim\Router;
use Slim\Collection;
use Slim\CallableResolver;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Headers;
use Slim\Http\Response;
use Slim\Handlers\PhpError;
use Slim\Handlers\Error;
use Slim\Handlers\NotAllowed;
use Tobinho\Mealtime\Infrastructure\Slim\AutoWiringStrategy;

class SlimServiceProvider extends AbstractServiceProvider
{
    /**
     * @var array
     */
    protected $provides = [
        'settings',
        'environment',
        'request',
        'response',
        'router',
        'foundHandler',
        'phpErrorHandler',
        'errorHandler',
        'notFoundHandler',
        'notAllowedHandler',
        'callableResolver',
    ];

    /**
     * @var array
     */
    protected $aliases = [
        Collection::class => 'settings',
        Request::class => 'request',
        RequestInterface::class => 'request',
        ServerRequestInterface::class => 'request',
        Response::class => 'response',
        ResponseInterface::class => 'response',
        Router::class => 'router',
    ];

    /**
     * @var array
     */
    private $defaultSettings = [
        'httpVersion' => '1.1',
        'responseChunkSize' => 4096,
        'outputBuffering' => 'append',
        'determineRouteBeforeAppMiddleware' => true,
        'displayErrorDetails' => true,
    ];

    public function __construct()
    {
        $this->provides = array_merge($this->provides, array_keys($this->aliases));
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->container->share('settings', function () {
            return new Collection($this->defaultSettings);
        });

        $this->container->share('environment', function () {
            return new Environment($_SERVER);
        });

        $this->container->share('request', function () {
            return Request::createFromEnvironment($this->container->get('environment'));
        });

        $this->container->share('response', function () {
            $headers = new Headers(['Content-Type' => 'text/html']);
            $response = new Response(200, $headers);

            return $response->withProtocolVersion($this->container->get('settings')['httpVersion']);
        });

        $this->container->share('router', function () {
            $routerCacheFile = false;
            if (isset($this->container->get('settings')['routerCacheFile'])) {
                $routerCacheFile = $this->container->get('settings')['routerCacheFile'];
            }

            $router = (new Router)->setCacheFile($routerCacheFile);
            $router->setContainer($this->container);

            return $router;
        });

        $this->container->share('foundHandler', function () {
            return new AutoWiringStrategy($this->container);
        });

        $this->container->share('phpErrorHandler', function () {
            return new PhpError($this->container->get('settings')['displayErrorDetails']);
        });

        $this->container->share('errorHandler', function () {
            return new Error($this->container->get('settings')['displayErrorDetails']);
        });

        $this->container->share('notFoundHandler', function () {
            return function (Request $request, Response $response) {
                return $response
                    ->withStatus(404)
                    ->withHeader('Content-Type', 'application/json')
                    ->withJson([
                        'errors' => [
                            [
                                'code' => 404,
                                'reason' => 'Not found: '.$request->getUri(),
                            ],
                        ],
                    ]);
            };
        });

        $this->container->share('notAllowedHandler', function () {
            return new NotAllowed;
        });

        $this->container->share('callableResolver', function () {
            return new CallableResolver($this->container);
        });

        // Register aliases.
        foreach ($this->aliases as $alias => $definition) {
            $this->container->share($alias, function () use ($definition) {
                return $this->container->get($definition);
            });
        }
    }
}

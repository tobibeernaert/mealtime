<?php

namespace Tobinho\Mealtime\Application\Provider;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Tobinho\Mealtime\Domain\Unit\UnitFactory;

class UnitServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        UnitFactory::class
    ];

    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->container->share(UnitFactory::class, function () {
            return new UnitFactory();
        });
    }
}

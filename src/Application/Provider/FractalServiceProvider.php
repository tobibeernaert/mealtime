<?php

namespace Tobinho\Mealtime\Application\Provider;

use League\Container\ServiceProvider\AbstractServiceProvider;
use League\Fractal\Manager;

class FractalServiceProvider extends AbstractServiceProvider
{
    /**
     * @var array
     */
    protected $provides = [
        Manager::class,
    ];

    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->container->add(Manager::class, function () {
            return new Manager;
        });
    }
}

<?php

namespace Tobinho\Mealtime\Application\Provider;

use League\Container\ServiceProvider\AbstractServiceProvider;
use League\Container\ServiceProvider\BootableServiceProviderInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\JsonFormatter;

class LoggingServiceProvider extends AbstractServiceProvider implements BootableServiceProviderInterface
{
    protected $provides = [
        'null_logger',
        'mealtime_logger',
        LoggerInterface::class,
        Logger::class,
    ];

    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->container->add('null_logger', function () {
            return new NullLogger();
        });

        $this->container->add('mealtime_logger', function () {
            $logger = new Logger('mealtime');

            // Add some extras
            $logger->pushProcessor(new IntrospectionProcessor(Logger::DEBUG));

            // Write messages to stdout
            $stdoutStream = new StreamHandler(
                'php://stdout',
                getenv('MINIMUM_LOG_LEVEL')
            );
            $stdoutStream->setFormatter(new JsonFormatter());
            $logger->pushHandler($stdoutStream);

            // Write errors to stderr
            $stderrStream = new StreamHandler('php://stderr', Logger::ERROR);
            $stderrStream->setFormatter(new JsonFormatter());
            $logger->pushHandler($stderrStream);

            return $logger;
        });

        $this->container->add(LoggerInterface::class, function () {
            return $this->container->get(Logger::class);
        });

        $this->container->add(Logger::class, function () {
            return $this->container->get(getenv('LOGGER_NAME'));
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->container->inflector(LoggerAwareInterface::class, function (LoggerAwareInterface $loggerAware) {
            $loggerAware->setLogger($this->container->get(LoggerInterface::class));
        });
    }
}

<?php

namespace Tobinho\Mealtime\Application\Provider;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Tobinho\Mealtime\Application\Http\Middleware\CorsMiddleware;

class HttpServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        CorsMiddleware::class,
    ];

    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->container->add(CorsMiddleware::class, function () {
            return new CorsMiddleware(getenv('ALLOWED_ORIGIN'));
        });
    }
}

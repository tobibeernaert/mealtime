<?php

namespace Tobinho\Mealtime\Application\Provider;

use League\Container\ServiceProvider\AbstractServiceProvider;
use League\Tactician\CommandBus;
use League\Tactician\Container\ContainerLocator;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\MethodNameInflector\HandleClassNameWithoutSuffixInflector;
use Tobinho\Mealtime\Application\Meal\Command\AddMealCommand;
use Tobinho\Mealtime\Application\Meal\CommandHandler\MealCommandHandler;

class CommandBusServiceProvider extends AbstractServiceProvider
{
    /**
     * @var array
     */
    protected $provides = [
        CommandBus::class,
        'commandBus',
    ];

    /**
     * @var array
     */
    protected $commandToHandler = [
        AddMealCommand::class => MealCommandHandler::class
    ];

    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->container->add(CommandBus::class, function () {
            $handlerMiddleware = new CommandHandlerMiddleware(
                new ClassNameExtractor,
                new ContainerLocator(
                    $this->container,
                    $this->commandToHandler
                ),
                new HandleClassNameWithoutSuffixInflector
            );

            return new CommandBus([$handlerMiddleware]);
        });

        $this->container->add('commandBus', function () {
            return $this->container->get(CommandBus::class);
        });
    }
}

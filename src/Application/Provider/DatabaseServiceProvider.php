<?php

namespace Tobinho\Mealtime\Application\Provider;

use PDO;
use League\Container\ServiceProvider\AbstractServiceProvider;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\Common\Cache\ApcuCache;
use Doctrine\DBAL\Logging\EchoSQLLogger;
use Doctrine\DBAL\Types\Type;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Meal\MealId as MealIdEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Meal\MealName as MealNameEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Meal\MealType as MealTypeEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Date as DateEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\StrictUrl as StrictUrlEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Author\AuthorName as AuthorNameEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Author\AuthorId as AuthorIdEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Recipe\RecipeName as RecipeNameEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Recipe\RecipeId as RecipeIdEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Ingredient\IngredientName as IngredientNameEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Ingredient\IngredientId as IngredientIdEntityType;
use Tobinho\Mealtime\Application\Storage\Orm\Types\Unit\Unit as UnitEntityType;
use Tobinho\Mealtime\Domain\Meal\MealRepository;
use Tobinho\Mealtime\Domain\Meal\Meal;
use Tobinho\Mealtime\Domain\Author\AuthorRepository;
use Tobinho\Mealtime\Domain\Author\Author;
use Tobinho\Mealtime\Domain\Recipe\RecipeRepository;
use Tobinho\Mealtime\Domain\Recipe\Recipe;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientRepository;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Ingredient;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Tobinho\Mealtime\Infrastructure\Doctrine\Listener\NullableEmbeddableListener;

class DatabaseServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        'pdo.mealtime',
        EntityManager::class,
        MealRepository::class,
        AuthorRepository::class,
        RecipeRepository::class,
        IngredientRepository::class,
    ];

    private $customEntityTypes = [
        MealIdEntityType::NAME => MealIdEntityType::class,
        MealNameEntityType::NAME => MealNameEntityType::class,
        MealTypeEntityType::NAME => MealTypeEntityType::class,
        AuthorNameEntityType::NAME => AuthorNameEntityType::class,
        AuthorIdEntityType::NAME => AuthorIdEntityType::class,
        RecipeNameEntityType::NAME => RecipeNameEntityType::class,
        RecipeIdEntityType::NAME => RecipeIdEntityType::class,
        IngredientNameEntityType::NAME => IngredientNameEntityType::class,
        IngredientIdEntityType::NAME => IngredientIdEntityType::class,
        UnitEntityType::NAME  => UnitEntityType::class,
        DateEntityType::NAME => DateEntityType::class,
        StrictUrlEntityType::NAME => StrictUrlEntityType::class,
    ];

    public function register()
    {
        $this->container->share('pdo.mealtime', function () {
            return new PDO(
                sprintf('mysql:host=%s;dbname=%s', getenv('MYSQL_MEALTIME_HOST'), getenv('MYSQL_MEALTIME_DB')),
                getenv('MYSQL_MEALTIME_USER'),
                getenv('MYSQL_MEALTIME_PASSWORD')
            );
        });

        $this->addCustomEntityTypes();

        $this->container->add(Configuration::class, function () {
            $configuration = Setup::createYAMLMetadataConfiguration(
                [__DIR__.'/../Storage/Orm/Mapping',], // maybe use basepath from Application ??
                getenv('DEBUG'),
                __DIR__.'/../../../database/proxies', // proxy directory
                (!getenv('DEBUG') && extension_loaded('apcu')) ? new ApcuCache() : null
            );

            // If you want to go bananas with logging ...
            // TODO use DebugStack to supress logging
            //$configuration->setSQLLogger(new EchoSQLLogger());

            $configuration->setProxyNamespace("Teamleader\Payments\Proxies");

            // Always generate proxy classes in debug mode
            if (getenv('DEBUG')) {
                $configuration->setAutoGenerateProxyClasses(true);
            }

            $configuration->setNamingStrategy(new UnderscoreNamingStrategy());

            // Add entity listeners
            //$configuration->getEntityListenerResolver()->register(new IngredientListener());
            $configuration
                ->getEntityListenerResolver()
                ->register($this->container->get(NullableEmbeddableListener::class));

            return $configuration;
        });

        $this->container->share(NullableEmbeddableListener::class, function () {
            $listener = new NullableEmbeddableListener(new PropertyAccessor());
            $listener->addMapping(Ingredient::class, 'amount');

            return $listener;
        });

        $this->container->share(Connection::class, function () {
            return DriverManager::getConnection(
                [
                    'driver' => 'pdo_mysql',
                    'pdo' => $this->container->get('pdo.mealtime'),
                ],
                $this->container->get(Configuration::class)
            );
        });

        $this->container->share(EntityManager::class, function() {
            return EntityManager::create(
                $this->container->get(Connection::class),
                $this->container->get(Configuration::class)
            );
        });

        $this->container->share(MealRepository::class, function() {
            return $this->container->get(EntityManager::class)->getRepository(Meal::class);
        });

        $this->container->share(AuthorRepository::class, function() {
            return $this->container->get(EntityManager::class)->getRepository(Author::class);
        });

        $this->container->share(RecipeRepository::class, function() {
            return $this->container->get(EntityManager::class)->getRepository(Recipe::class);
        });

        $this->container->share(IngredientRepository::class, function() {
            return $this->container->get(EntityManager::class)->getRepository(Ingredient::class);
        });
    }

    private function addCustomEntityTypes()
    {
        array_walk($this->customEntityTypes, function ($entityTypeClass, $entityTypeName) {
            if (!Type::hasType($entityTypeName)) {
                Type::addType($entityTypeName, $entityTypeClass);
            }
        });
    }
}

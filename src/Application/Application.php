<?php

namespace Tobinho\Mealtime\Application;

use Slim\App as Slim;
use Dotenv\Dotenv;
use League\Container\Container;
use League\Container\ReflectionContainer;
use Tobinho\Mealtime\Application\Provider\SlimServiceProvider;
use Tobinho\Mealtime\Application\Provider\LoggingServiceProvider;
use Tobinho\Mealtime\Application\Provider\DatabaseServiceProvider;
use Tobinho\Mealtime\Application\Provider\CommandBusServiceProvider;
use Tobinho\Mealtime\Application\Provider\FractalServiceProvider;
use Tobinho\Mealtime\Application\Provider\HttpServiceProvider;
use Tobinho\Mealtime\Application\Provider\UnitServiceProvider;

class Application extends Slim
{
    /**
     * @var string
     */
    private $basePath;

    public function __construct(string $basePath)
    {
        $this->basePath = $basePath;

        try {
            (new Dotenv($this->basePath))->load();
        } catch (\Exception $e) {
            // ...
        }

        // Call parent constructor with custom container.
        $container = $this->bootstrapContainer();
        parent::__construct($container);

        // Load routes.
        $this->loadRoutes();
    }

    public function bootstrapContainer(): Container
    {
        $container = new Container();
        $container->delegate(new ReflectionContainer());
        $container->addServiceProvider(new SlimServiceProvider());
        $container->addServiceProvider(new LoggingServiceProvider());
        $container->addServiceProvider(new DatabaseServiceProvider());
        $container->addServiceProvider(new CommandBusServiceProvider());
        $container->addServiceProvider(new FractalServiceProvider());
        $container->addServiceProvider(new HttpServiceProvider());
        $container->addServiceProvider(new UnitServiceProvider());

        return $container;
    }

    public function loadRoutes()
    {
        $app = $this;

        require __DIR__ . '/Http/routes.php';
    }
}

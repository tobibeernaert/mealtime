<?php

namespace Tobinho\Mealtime\Application\Meal\CommandHandler;

use Tobinho\Mealtime\Application\Meal\Command\AddMealCommand;
use Tobinho\Mealtime\Domain\Meal\MealRepository;

class MealCommandHandler
{
    /**
     * @var MealRepository
     */
    private $mealRepository;

    public function __construct(MealRepository $mealRepository)
    {
        $this->mealRepository = $mealRepository;
    }

    public function handleAddMeal(AddMealCommand $command)
    {
        $this->mealRepository->save($command->getMeal());
    }
}

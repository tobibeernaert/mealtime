<?php

namespace Tobinho\Mealtime\Application\Meal\Transformer;

use League\Fractal\TransformerAbstract;
use Tobinho\Mealtime\Domain\Meal\Meal;

class MealTransformer extends TransformerAbstract
{
    /**
     * {@inheritDoc}
     */
    public function transform(Meal $meal)
    {
        return [
            'id' => $meal->getId()->id(),
            'date' => $meal->getDate()->format('Y-m-d'),
            'type' => $meal->getType()->getValue(),
            'name' => $meal->getName(),
            'recipe_url' => $meal->getRecipeUrl(),
        ];
    }
}

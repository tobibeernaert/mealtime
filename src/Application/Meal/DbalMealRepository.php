<?php

namespace Tobinho\Mealtime\Application\Meal;

use Doctrine\ORM\EntityRepository;
use Tobinho\Mealtime\Domain\Meal\MealRepository;
use Tobinho\Mealtime\Domain\Meal\Meal;
use Tobinho\Mealtime\Infrastructure\ValueObject\Date\Date;

class DbalMealRepository extends EntityRepository implements MealRepository
{
    final public function save(Meal $meal)
    {
        $this->_em->persist($meal);
        $this->_em->flush($meal);
    }

    final public function delete(Meal $meal)
    {
        $this->_em->remove($meal);
        $this->_em->flush($meal);
    }

    final public function mealsForDay(Date $date): array
    {
        return $this->findBy(['date' => $date]);
    }

    final public function mealsForPeriod(Date $startDate, Date $endDate): array
    {
        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->select('m');
        $queryBuilder->from(Meal::class, 'm');
        $queryBuilder->where('m.date BETWEEN :startDate AND :endDate');
        $queryBuilder->setParameter('startDate', $startDate->format('Y-m-d'));
        $queryBuilder->setParameter('endDate', $endDate->format('Y-m-d'));

        return $queryBuilder->getQuery()->getResult();
    }

    public function mealsForToday(): array
    {
        return $this->mealsForDay(Date::now());
    }

    public function mealsForTomorrow(): array
    {
        $tomorrow = Date::now()->addDay();
        return $this->mealsForDay($tomorrow);
    }

    public function mealsForYesterday(): array
    {
        $yesterday = Date::now()->subDay();
        return $this->mealsForDay($yesterday);
    }

    public function mealsForThisWeek(): array
    {
        $startOfWeek = (Date::now())->startOfWeek();
        $endOfWeek = (Date::now())->endOfWeek();

        return $this->mealsForPeriod($startOfWeek, $endOfWeek);
    }

    public function mealsForNextWeek(): array
    {
        $startOfNextWeek = (Date::now())->startOfNextWeek();
        $endOfNextWeek = (Date::now())->endOfNextWeek();

        return $this->mealsForPeriod($startOfNextWeek, $endOfNextWeek);
    }

    public function mealsForLastWeek(): array
    {
        $startOfLastWeek = (Date::now())->startOfLastWeek();
        $endOfLastWeek = (Date::now())->endOfLastWeek();

        return $this->mealsForPeriod($startOfLastWeek, $endOfLastWeek);
    }
}

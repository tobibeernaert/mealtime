<?php

namespace Tobinho\Mealtime\Application\Meal\Command;

use Tobinho\Mealtime\Domain\Meal\Meal;

class AddMealCommand
{
    /**
     * @var meal
     */
    private $meal;

    public function __construct(Meal $meal)
    {
        $this->meal = $meal;
    }

    public function getMeal(): Meal
    {
        return $this->meal;
    }
}

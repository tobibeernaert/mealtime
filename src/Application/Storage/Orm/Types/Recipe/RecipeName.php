<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types\Recipe;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class RecipeName extends StringType
{
    const NAME = "RecipeName";

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     *
     * @return RecipeName
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new \Tobinho\Mealtime\Domain\Recipe\RecipeName($value);
    }

    /**
     * @param \Tobinho\Mealtime\Domain\Recipe\RecipeName $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

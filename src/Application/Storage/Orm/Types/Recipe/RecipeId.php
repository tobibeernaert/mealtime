<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types\Recipe;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;

class RecipeId extends GuidType
{
    const NAME = "RecipeId";

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     *
     * @return RecipeName
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new \Tobinho\Mealtime\Domain\Recipe\RecipeId($value);
    }

    /**
     * @param \Tobinho\Mealtime\Domain\Recipe\RecipeId $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->id();
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

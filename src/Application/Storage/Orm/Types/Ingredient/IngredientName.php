<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types\Ingredient;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class IngredientName extends StringType
{
    const NAME = "IngredientName";

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     *
     * @return IngredientName
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new \Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientName($value);
    }

    /**
     * @param \Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientName $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types\Unit;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class Unit extends StringType
{
    const NAME = "Unit";

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     *
     * @return RecipeName
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        [$className, $unit] = explode('::', $value);

        return new $className($unit);
    }

    /**
     * @param \Tobinho\Mealtime\Domain\Unit\Unit $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value
            ? get_class($value).'::'.$value->getUnit()
            : null;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

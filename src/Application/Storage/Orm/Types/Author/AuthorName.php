<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types\Author;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class AuthorName extends StringType
{
    const NAME = "AuthorName";

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     *
     * @return AuthorName
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new \Tobinho\Mealtime\Domain\Author\AuthorName($value);
    }

    /**
     * @param \Tobinho\Mealtime\Domain\Author\AuthorName $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

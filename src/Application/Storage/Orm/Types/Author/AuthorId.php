<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types\Author;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;

class AuthorId extends GuidType
{
    const NAME = "AuthorId";

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     *
     * @return AuthorName
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new \Tobinho\Mealtime\Domain\Author\AuthorId($value);
    }

    /**
     * @param \Tobinho\Mealtime\Domain\Author\AuthorId $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->id();
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

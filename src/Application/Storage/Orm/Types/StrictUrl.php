<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class StrictUrl extends StringType
{
    const NAME = "StrictUrl";

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     *
     * @return \Tobinho\Mealtime\Infrastructure\ValueObject\Url\StrictUrl
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return null !== $value
            ? new \Tobinho\Mealtime\Infrastructure\ValueObject\Url\StrictUrl($value)
            : null;
    }

    /**
     * @param \Tobinho\Mealtime\Infrastructure\ValueObject\Url\StrictUrl $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types\Meal;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class MealType extends StringType
{
    const NAME = "MealType";

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     *
     * @return \Tobinho\Mealtime\Domain\Meal\MealType
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new \Tobinho\Mealtime\Domain\Meal\MealType($value);
    }

    /**
     * @param \Tobinho\Mealtime\Domain\Meal\MealType $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

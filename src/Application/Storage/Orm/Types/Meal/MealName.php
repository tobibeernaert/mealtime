<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types\Meal;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class MealName extends StringType
{
    const NAME = "MealName";

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     *
     * @return MealName
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new \Tobinho\Mealtime\Domain\Meal\MealName($value);
    }

    /**
     * @param \Tobinho\Mealtime\Domain\Meal\MealName $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

<?php

namespace Tobinho\Mealtime\Application\Storage\Orm\Types;

use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Tobinho\Mealtime\Infrastructure\ValueObject\Date\Date as DateObject;
use DateTime;

class Date extends DateType
{
    const NAME = "Date";

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        $dateTime = parent::convertToPHPValue($value, $platform);

        return DateObject::fromDateTime($dateTime);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof DateObject) {
            return $value->format($platform->getDateFormatString());
        }

        return parent::convertToDatabaseValue($value, $platform);
    }

    public function getName(): string
    {
        return static::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

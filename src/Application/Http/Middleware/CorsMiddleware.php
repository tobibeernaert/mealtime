<?php

namespace Tobinho\Mealtime\Application\Http\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;

class CorsMiddleware
{
    /**
     * @var string
     */
    private $allowedOrigin;

    public function __construct(string $allowedOrigin)
    {
        $this->allowedOrigin = $allowedOrigin;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response, callable $next
    ): ResponseInterface {
        if ($request->getMethod() === 'OPTIONS') {
            return $this->applyCorsHeaders(new Response());
        }

        return $this->applyCorsHeaders(
            $next($request, $response)
        );
    }

    private function applyCorsHeaders(ResponseInterface $response): ResponseInterface
    {
        return $response
            ->withHeader('Access-Control-Allow-Origin', $this->allowedOrigin)
            ->withHeader(
                'Access-Control-Allow-Headers',
                'X-Requested-With, Content-Type, Accept, Origin, Authorization'
            )
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }
}

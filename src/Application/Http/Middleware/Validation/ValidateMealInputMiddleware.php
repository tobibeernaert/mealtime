<?php

namespace Tobinho\Mealtime\Application\Http\Middleware\Validation;

use Slim\Http\Request;
use Slim\Http\Response;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\ValidationException;

class ValidateMealInputMiddleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $mealData = $request->getParsedBody();

        try {
            $mealDataValidator = v::arrayVal()
                ->key('date', v::stringType()->notEmpty())
                ->key('type', v::stringType()->notEmpty())
                ->key('name', v::stringType()->notEmpty())
                ;
            $mealDataValidator->check($mealData);

        } catch (ValidationException $exception) {
            return $response
                ->withStatus(400)
                ->withJson(['Invalid format in request']);
        }

        return $next($request, $response);
    }
}

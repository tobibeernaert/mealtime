<?php

namespace Tobinho\Mealtime\Application\Http\Middleware\Validation;

use Slim\Http\Request;
use Slim\Http\Response;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\ValidationException;

class ValidateAuthorInputMiddleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $authorData = $request->getParsedBody();

        try {
            $authorDataValidator = v::arrayVal()
                ->key('name', v::stringType()->notEmpty())
                ->key('website', v::stringType())
                ;
            $authorDataValidator->check($authorData);

        } catch (ValidationException $exception) {
            return $response
                ->withStatus(400)
                ->withJson(['Invalid format in request']);
        }

        return $next($request, $response);
    }
}

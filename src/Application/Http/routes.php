<?php

namespace Tobinho\Mealtime\Application\Http;

use Tobinho\Mealtime\Application\Http\Controller\Api\PingAction;
use Tobinho\Mealtime\Application\Http\Controller\CorsAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Meals\AddAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Meals\TodayAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Meals\TomorrowAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Meals\YesterdayAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Meals\WeekAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Meals\NextWeekAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Meals\LastWeekAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Authors\ListAction as AuthorListAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Authors\DeleteAction as AuthorDeleteAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Authors\AddAction as AuthorAddAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Authors\AuthorAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Recipes\ListAction as RecipeListAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Recipes\DeleteAction as RecipeDeleteAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Recipes\AddAction as RecipeAddAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Unit\ListAction as UnitListAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Unit\MassListAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Unit\VolumeListAction;
use Tobinho\Mealtime\Application\Http\Controller\Api\Unit\SizeListAction;
use Tobinho\Mealtime\Application\Http\Middleware\Validation\ValidateMealInputMiddleware;
use Tobinho\Mealtime\Application\Http\Middleware\Validation\ValidateAuthorInputMiddleware;
use Tobinho\Mealtime\Application\Http\Middleware\CorsMiddleware;

$app->group('/api', function () use ($app) {
    $app->get('/ping', PingAction::class);

    // Meals
    $app->post('/meals', AddAction::class)
        ->add(ValidateMealInputMiddleware::class);
    $app->get('/meals/today', TodayAction::class);
    $app->get('/meals/tomorrow', TomorrowAction::class);
    $app->get('/meals/yesterday', YesterdayAction::class);
    $app->get('/meals/week', WeekAction::class);
    $app->get('/meals/week/next', NextWeekAction::class);
    $app->get('/meals/week/last', LastWeekAction::class);

    // Authors
    $app->get('/authors', AuthorListAction::class);
    $app->get('/authors/{id}', AuthorAction::class);
    $app->delete('/authors/{id}', AuthorDeleteAction::class);
    $app->post('/authors', AuthorAddAction::class)
        ->add(ValidateAuthorInputMiddleware::class);

    // Recipes
    $app->get('/recipes', RecipeListAction::class);
    $app->delete('/recipes/{id}', RecipeDeleteAction::class);
    $app->post('/recipes', RecipeAddAction::class);

    // Units
    $app->get('/units', UnitListAction::class);
    $app->get('/units/mass', MassListAction::class);
    $app->get('/units/volume', VolumeListAction::class);
    $app->get('/units/size', SizeListAction::class);
});

$app->add(CorsMiddleware::class);

<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Authors;

use Slim\Http\Request;
use Slim\Http\Response;
use Tobinho\Mealtime\Domain\Author\AuthorRepository;
use Tobinho\Mealtime\Domain\Author\Author;
use Tobinho\Mealtime\Domain\Author\AuthorId;
use Tobinho\Mealtime\Domain\Author\AuthorName;
use Tobinho\Mealtime\Infrastructure\ValueObject\Url\StrictUrl;

class AddAction
{
    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $authorData = $request->getParsedBody();
        $author = new Author(
            new AuthorId(),
            new AuthorName($authorData['name']),
            !empty($authorData['website']) ? new StrictUrl($authorData['website']) : null
        );
        $this->authorRepository->save($author);

        return $response->withStatus(204);
    }
}

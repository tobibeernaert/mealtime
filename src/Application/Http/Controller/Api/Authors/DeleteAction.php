<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Authors;

use Slim\Http\Request;
use Slim\Http\Response;
use Tobinho\Mealtime\Domain\Author\AuthorRepository;
use Tobinho\Mealtime\Domain\Author\AuthorId;

class DeleteAction
{
    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $route = $request->getAttribute('route');
        $arguments = $route->getArguments();

        $author = $this->authorRepository->one(new AuthorId($arguments['id']));
        $this->authorRepository->delete($author);

        return $response->withStatus(200);
    }
}

<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Authors;

use Slim\Http\Request;
use Slim\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Tobinho\Mealtime\Domain\Author\AuthorRepository;
use Tobinho\Mealtime\Application\Author\Transformer\AuthorTransformer;

class ListAction
{
    /**
     * @var Manager
     */
    private $fractalManager;

    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    public function __construct(
        Manager $fractalManager,
        AuthorRepository $authorRepository
    ) {
        $this->fractalManager = $fractalManager;
        $this->authorRepository = $authorRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $authors = $this->authorRepository->all();

        $authorCollection = new Collection($authors, new AuthorTransformer());
        $authorCollectionAsArray = $this->fractalManager->createData($authorCollection)->toArray();

        return $response->withJson($authorCollectionAsArray);
    }
}

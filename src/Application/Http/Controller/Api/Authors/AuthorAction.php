<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Authors;

use Slim\Http\Request;
use Slim\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Tobinho\Mealtime\Domain\Author\AuthorRepository;
use Tobinho\Mealtime\Application\Author\Transformer\AuthorTransformer;
use Tobinho\Mealtime\Domain\Author\AuthorId;

class AuthorAction
{
    /**
     * @var Manager
     */
    private $fractalManager;

    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    public function __construct(
        Manager $fractalManager,
        AuthorRepository $authorRepository
    ) {
        $this->fractalManager = $fractalManager;
        $this->authorRepository = $authorRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $route = $request->getAttribute('route');
        $arguments = $route->getArguments();
        $author = $this->authorRepository->one(new AuthorId($arguments['id']));

        $authorItem = new Item($author, new AuthorTransformer());
        $authorItemAsArray = $this->fractalManager->createData($authorItem)->toArray();

        return $response->withJson($authorItemAsArray);
    }
}

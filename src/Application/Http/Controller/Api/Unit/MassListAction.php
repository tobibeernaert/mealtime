<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Unit;

use Slim\Http\Request;
use Slim\Http\Response;
use Tobinho\Mealtime\Domain\Unit\Mass;

class MassListAction
{
    public function __invoke(Request $request, Response $response): Response
    {
        return $response->withJson(Mass::getUnits());
    }
}

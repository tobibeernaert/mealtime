<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Meals;

use Slim\Http\Request;
use Slim\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Tobinho\Mealtime\Domain\Meal\MealRepository;
use Tobinho\Mealtime\Application\Meal\Transformer\MealTransformer;
use Tobinho\Mealtime\Infrastructure\ValueObject\Date\Date;

class NextWeekAction
{
    /**
     * @var Manager
     */
    private $fractalManager;

    /**
     * @var MealRepository
     */
    private $mealRepository;

    public function __construct(Manager $fractalManager, MealRepository $mealRepository)
    {
        $this->fractalManager = $fractalManager;
        $this->mealRepository = $mealRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $meals = $this->mealRepository->mealsForNextWeek();

        $mealCollection = new Collection($meals, new MealTransformer);
        $mealCollectionAsArray = $this->fractalManager->createData($mealCollection)->toArray();

        return $response->withJson($mealCollectionAsArray);
    }
}

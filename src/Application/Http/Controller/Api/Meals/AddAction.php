<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Meals;

use League\Tactician\CommandBus;
use Slim\Http\Request;
use Slim\Http\Response;
use Tobinho\Mealtime\Domain\Meal\Meal;
use Tobinho\Mealtime\Domain\Meal\MealType;
use Tobinho\Mealtime\Domain\Meal\MealName;
use Tobinho\Mealtime\Domain\Meal\RecipeUrl;
use Tobinho\Mealtime\Infrastructure\ValueObject\Date\Date;
use Tobinho\Mealtime\Application\Meal\Command\AddMealCommand;

class AddAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $meal = new Meal(
            Date::now(),
            MealType::lunch(),
            new MealName("Recipe 01"),
            new RecipeUrl("http://www.example.com/recipe_01")
        );

        $command = new AddMealCommand($meal);
        $this->commandBus->handle($command);

        return $response;
    }
}

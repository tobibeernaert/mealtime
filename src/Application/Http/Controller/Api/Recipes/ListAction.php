<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Recipes;

use Slim\Http\Request;
use Slim\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Tobinho\Mealtime\Domain\Recipe\RecipeRepository;
use Tobinho\Mealtime\Application\Recipe\Transformer\RecipeTransformer;

class ListAction
{
    /**
     * @var Manager
     */
    private $fractalManager;

    /**
     * @var RecipeRepository
     */
    private $recipeRepository;

    public function __construct(
        Manager $fractalManager,
        RecipeRepository $recipeRepository
    ) {
        $this->fractalManager = $fractalManager;
        $this->recipeRepository = $recipeRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $recipes = $this->recipeRepository->all();

        $recipeCollection = new Collection($recipes, new RecipeTransformer());
        $recipeCollectionAsArray = $this->fractalManager->createData($recipeCollection)->toArray();

        return $response->withJson($recipeCollectionAsArray);
    }
}

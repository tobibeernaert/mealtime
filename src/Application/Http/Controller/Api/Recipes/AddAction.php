<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Recipes;

use Slim\Http\Request;
use Slim\Http\Response;
use Tobinho\Mealtime\Domain\Recipe\RecipeRepository;
use Tobinho\Mealtime\Domain\Author\AuthorRepository;
use Tobinho\Mealtime\Domain\Recipe\Recipe;
use Tobinho\Mealtime\Domain\Recipe\RecipeId;
use Tobinho\Mealtime\Domain\Recipe\RecipeName;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Ingredient;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientId;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientName;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Amount\Amount;
use Tobinho\Mealtime\Domain\Unit\UnitFactory;
use Tobinho\Mealtime\Domain\Author\AuthorId;
use Tobinho\Mealtime\Domain\Author\Author;
use Tobinho\Mealtime\Domain\Author\AuthorName;

class AddAction
{
    /**
     * @var RecipeRepository
     */
    private $recipeRepository;

    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    /**
     * @var UnitFactory
     */
    private $unitFactory;

    public function __construct(
        RecipeRepository $recipeRepository,
        AuthorRepository $authorRepository,
        UnitFactory $unitFactory
    ) {
        $this->recipeRepository = $recipeRepository;
        $this->authorRepository = $authorRepository;
        $this->unitFactory = $unitFactory;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $recipeData = $request->getParsedBody();

        $author = $this->authorRepository->one(new AuthorId($recipeData['author_id']));
        // TEMP
        if (null == $author) {
            $author = new Author(new AuthorId(), new AuthorName("TEMP author"));
        }

        $recipe = new Recipe(
            new RecipeId(),
            new RecipeName($recipeData['name']),
            $author
        );

        array_walk($recipeData['ingredients'], function ($ingredientData) use ($recipe) {
            $ingredient = new Ingredient(
                new IngredientId(),
                new IngredientName($ingredientData['name'])
            );
            if (isset($ingredientData['amount'])) {
                $amount = new Amount($ingredientData['amount']['value']);

                if (isset($ingredientData['amount']['unit'])) {
                    $this->unitFactory->get(
                        $ingredientData['amount']['type'],
                        $ingredientData['amount']['unit']
                    );
                }
                $ingredient->setAmount($amount);
            }
            $recipe->addIngredient($ingredient);
        });

        $this->recipeRepository->save($recipe);

        return $response->withJson(['ok']);
    }
}

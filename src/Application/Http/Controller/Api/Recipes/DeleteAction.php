<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api\Recipes;

use Slim\Http\Request;
use Slim\Http\Response;
use Tobinho\Mealtime\Domain\Recipe\RecipeRepository;
use Tobinho\Mealtime\Domain\Recipe\RecipeId;

class DeleteAction
{
    /**
     * @var RecipeRepository
     */
    private $recipeRepository;

    public function __construct(RecipeRepository $recipeRepository)
    {
        $this->recipeRepository = $recipeRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $route = $request->getAttribute('route');
        $arguments = $route->getArguments();

        $recipe = $this->recipeRepository->one(new RecipeId($arguments['id']));
        $this->recipeRepository->delete($recipe);

        return $response->withStatus(200);
    }
}

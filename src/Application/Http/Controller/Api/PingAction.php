<?php

namespace Tobinho\Mealtime\Application\Http\Controller\Api;

use Slim\Http\Request;
use Slim\Http\Response;
use Tobinho\Mealtime\Domain\Author\AuthorRepository;
use Tobinho\Mealtime\Domain\Author\Author;
use Tobinho\Mealtime\Domain\Author\AuthorId;
use Tobinho\Mealtime\Domain\Author\AuthorName;
use Tobinho\Mealtime\Domain\Recipe\RecipeRepository;
use Tobinho\Mealtime\Domain\Recipe\Recipe;
use Tobinho\Mealtime\Domain\Recipe\RecipeId;
use Tobinho\Mealtime\Domain\Recipe\RecipeName;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientRepository;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Ingredient;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientId;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientName;
use Tobinho\Mealtime\Domain\Unit\Mass;
use Tobinho\Mealtime\Domain\Unit\Volume;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Amount\Amount;

class PingAction
{
    private $authorRepository;
    private $recipeRepository;
    private $ingredientRepository;

    public function __construct(AuthorRepository $authorRepository, RecipeRepository $recipeRepository, IngredientRepository $ingredientRepository)
    {
        $this->authorRepository = $authorRepository;
        $this->recipeRepository = $recipeRepository;
        $this->ingredientRepository = $ingredientRepository;
    }

    public function __invoke(Request $request, Response $response)
    {
        //$ingredient = new Ingredient(
            //new IngredientId(),
            //new IngredientName("test")
        //);
        //$this->ingredientRepository->save($ingredient);

        // -----

        //$ingredient = new Ingredient(
            //new IngredientId(),
            //new IngredientName("test")
        //);
        //$amount = new Amount(1.2);
        //$amount->setUnit(Mass::kilogram());
        //$ingredient->setAmount($amount);
        //$this->ingredientRepository->save($ingredient);

        // -----

        //$a = new IngredientId();
        //$ingredient = new Ingredient(
            //$a,
            //new IngredientName("test")
        //);
        //$ingredient->setAmount(new Amount(5));
        //$this->ingredientRepository->save($ingredient);
        //$ingredient = $this->ingredientRepository->one($a);
        //var_dump($ingredient);

        // -----

        //$a = new IngredientId();
        //$ingredient = new Ingredient(
            //$a,
            //new IngredientName("test")
        //);
        //$amount = new Amount(3);
        //$amount->setUnit(Volume::teaspoon());
        //$ingredient->setAmount($amount);
        //$this->ingredientRepository->save($ingredient);

        //$ingredient = $this->ingredientRepository->one($a);
        //var_dump($ingredient);

        // -----

        $recipe = new Recipe(
            new RecipeId(),
            new RecipeName('recipe_01'),
            new Author(new AuthorId(), new AuthorName("Author"))
        );
        $ingredient = new Ingredient(new IngredientId(), new IngredientName("Onions"));
        $ingredient->setAmount(new Amount(2));
        $recipe->addIngredient($ingredient);

        $this->recipeRepository->save($recipe);

        return $response->withJson(["pong"]);
    }
}

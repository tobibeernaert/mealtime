<?php

namespace Tobinho\Mealtime\Application\Http\Controller;

use Slim\Http\Request;
use Slim\Http\Response;

class CorsAction
{
    public function __invoke(Request $request, Response $response): Response
    {
        $response->withHeader('Access-Control-Allow-Origin', '*');
        $response->withHeader(
            'Access-Control-Allow-Headers',
            'X-Requested-With, Content-Type, Accept, Origin, Authorization'
        );
        $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

        return $response;
    }
}

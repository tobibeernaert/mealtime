<?php

namespace Tobinho\Mealtime\Domain\Author;

use Tobinho\Mealtime\Infrastructure\ValueObject\String\NonEmptyStringLiteral;

class AuthorName extends NonEmptyStringLiteral
{
}

<?php

namespace Tobinho\Mealtime\Domain\Author;

use Tobinho\Mealtime\Domain\Author\Author;
use Tobinho\Mealtime\Domain\Author\AuthorId;

interface AuthorRepository
{
    public function save(Author $author);

    public function delete(Author $author);

    public function all(): array;

    public function one(AuthorId $authorId):? Author;
}

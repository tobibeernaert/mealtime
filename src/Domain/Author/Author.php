<?php

namespace Tobinho\Mealtime\Domain\Author;

use Tobinho\Mealtime\Domain\Author\AuthorName;
use Tobinho\Mealtime\Infrastructure\ValueObject\Url\StrictUrl;

class Author
{
    /**
     * @var AuthorId
     */
    private $id;

    /**
     * @var AuthorName
     */
    private $name;

    /**
     * @var StrictUrl
     */
    private $website;

    public function __construct(AuthorId $id, AuthorName $name, StrictUrl $website = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->website = $website;
    }

    public function getId(): AuthorId
    {
        return $this->id;
    }

    public function getName(): AuthorName
    {
        return $this->name;
    }

    public function setName(AuthorName $name)
    {
        $this->name = $name;
    }

    public function getWebsite():? StrictUrl
    {
        return $this->website;
    }

    public function setWebsite(StrictUrl $website)
    {
        $this->website = $website;
    }
}

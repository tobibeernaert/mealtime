<?php

namespace Tobinho\Mealtime\Domain\Author;

use Tobinho\Mealtime\Infrastructure\ValueObject\Identifier\UuidIdentifier;

class AuthorId extends UuidIdentifier
{
}

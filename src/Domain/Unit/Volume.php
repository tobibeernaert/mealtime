<?php

namespace Tobinho\Mealtime\Domain\Unit;

use Tobinho\Mealtime\Domain\Unit\Unit;

class Volume extends Unit
{
    const TYPE = 'volume';

    const BASE_UNIT = 'litre';

    const MILLILITRE = 'millilitre';
    const CENTILITRE = 'centilitre';
    const DECILITRE = 'decilitre';
    const LITRE = 'litre';
    const GALLON = 'gallon';
    const QUART = 'quart';
    const PINT = 'pint';
    const TABLESPOON = 'tablespoon';
    const TEASPOON = 'teaspoon';

    protected static $units = [
        self::MILLILITRE    => ['weight' => 0.001, 'short' => 'ml'],
        self::CENTILITRE    => ['weight' => 0.010, 'short' => 'cl'],
        self::DECILITRE     => ['weight' => 0.100, 'short' => 'dl'],
        self::LITRE         => ['weight' => 1.000, 'short' => 'l'],
        self::GALLON        => ['weight' => 3.78541, 'short' => 'gal'],
        self::QUART         => ['weight' => 0.946353, 'short' => 'qt'],
        self::PINT          => ['weight' => 0.473176, 'short' => 'pt'],
        self::TABLESPOON    => ['weight' => 0.015, 'short' => 'tbsp'],
        self::TEASPOON      => ['weight' => 0.005, 'short' => 'tsp'],
    ];
}

<?php

namespace Tobinho\Mealtime\Domain\Unit;

use Tobinho\Mealtime\Domain\Unit\Guards\GuardUnitExists;

abstract class Unit
{
    use GuardUnitExists;

    /**
     * @var string
     */
    private $unit;

    final public function __construct(string $unit = null)
    {
        if (null === $unit) {
            $unit = static::BASE_UNIT;
        }
        $this->validateUnit($unit);

        $this->unit = $unit;
    }

    final public function getUnit(): string
    {
        return $this->unit;
    }

    final public function getUnitShort(): string
    {
        return $this->units[$this->unit]['short'];
    }

    public function convert(float $value, string $unit): float
    {
        $this->validateUnit($unit);

        $currentUnit = $this->units[$this->unit];
        $baseValue = $value * $currentUnit['weight'];
        $newUnit = $this->units[$unit];

        return $baseValue / $newUnit['weight'];
    }

    final public static function __callStatic($methodName, $arguments): Unit
    {
        return new static($methodName);
    }

    public static function getUnits(): array
    {
        return array_keys(static::$units);
    }

    final public function getType()
    {
        return self::NAME;
    }

    public function __toString(): string
    {
        return $this->getUnitShort();
    }
}

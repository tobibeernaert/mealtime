<?php

namespace Tobinho\Mealtime\Domain\Unit\Guards;

use InvalidArgumentException;

trait GuardUnitExists
{
    public function validateUnit(string $unit)
    {
        if (!isset(static::$units[$unit])) {
            throw new InvalidArgumentException("Unit $unit does not exists!");
        }
    }
}

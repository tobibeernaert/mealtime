<?php

namespace Tobinho\Mealtime\Domain\Unit;

use Tobinho\Mealtime\Domain\Unit\Unit;

class Numeric extends Unit
{
    const TYPE = 'numeric';

    const BASE_UNIT = 'numeric';

    const NUMERIC = 'numeric';

    protected $units = [
        self::NUMERIC => ['weight' => 1, 'short' => ''],
    ];
}

<?php

namespace Tobinho\Mealtime\Domain\Unit;

use Tobinho\Mealtime\Domain\Unit\Unit;

class Size extends Unit
{
    const TYPE = 'size';

    const BASE_UNIT = 'normal';

    const EXTRA_SMALL = 'extra_small';
    const SMALL = 'small';
    const NORMAL = 'normal';
    const LARGE = 'large';
    const EXTRA_LARGE = 'extra_large';

    protected static $units = [
        self::EXTRA_SMALL => ['weight' => 2, 'short' => 'extra-small'],
        self::SMALL => ['weight' => 1.5, 'short' => 'small'],
        self::NORMAL => ['weight' => 1, 'short' => 'normal'],
        self::LARGE => ['weight' => 0.5, 'short' => 'large'],
        self::EXTRA_LARGE => ['weight' => 0.1, 'short' => 'extra-large'],
    ];
}

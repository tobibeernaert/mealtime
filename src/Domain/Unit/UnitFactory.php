<?php

namespace Tobinho\Mealtime\Domain\Unit;

use Tobinho\Mealtime\Domain\Unit\Volume;
use Tobinho\Mealtime\Domain\Unit\Mass;
use Tobinho\Mealtime\Domain\Unit\Size;
use Tobinho\Mealtime\Domain\Unit\Unit;

class UnitFactory
{
    public function volume(string $volumeUnit): Volume
    {
        return new Volume($volumeUnit);
    }

    public function mass(string $massUnit): Mass
    {
        return new Mass($massUnit);
    }

    public function size(string $sizeUnit): Size
    {
        return new Size($sizeUnit);
    }

    public function get(string $unitType, string $unit): Unit
    {
        switch ($unitType) {
            case 'volume':
                return $this->volume($unit);

            case 'mass':
                return $this->mass($unit);

            case 'size':
                return $this->size($unit);
        }

        throw new InvalidArgumentException("$unitType does not exist");
    }
}

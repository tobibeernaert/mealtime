<?php

namespace Tobinho\Mealtime\Domain\Unit;

use Tobinho\Mealtime\Domain\Unit\Unit;

class Mass extends Unit
{
    const TYPE = 'mass';

    const BASE_UNIT = 'kilogram';

    const GRAM = 'gram';
    const KILOGRAM = 'kilogram';
    const OUNCE = 'ounce';
    const POUND = 'pound';
    const TEASPOON = 'teaspoon';
    const TABLESPOON = 'tablespoon';

    protected static $units = [
        self::GRAM          => ['weight' => 0.001, 'short' => 'gr'],
        self::KILOGRAM      => ['weight' => 1.000, 'short' => 'kg'],
        self::OUNCE         => ['weight' => 0.02834952, 'short' => 'oz'],
        self::POUND         => ['weight' => 0.45359237, 'short' => 'lb'],
        self::TABLESPOON    => ['weight' => 0.01478676, 'short' => 'tbsp'],
        self::TEASPOON      => ['weight' => 0.0043, 'short' => 'tsp'], // don't know about this tho ...
        // TODO :: cups are based on density of the content > need to be linked with an ingredient, rather than a unit.
    ];
}

<?php

namespace Tobinho\Mealtime\Domain\Recipe;

use Tobinho\Mealtime\Infrastructure\ValueObject\String\NonEmptyStringLiteral;

class RecipeName extends NonEmptyStringLiteral
{
}

<?php

namespace Tobinho\Mealtime\Domain\Recipe;

use Tobinho\Mealtime\Domain\Recipe\RecipeId;
use Tobinho\Mealtime\Domain\Recipe\RecipeName;
use Tobinho\Mealtime\Domain\Author\Author;
use Tobinho\Mealtime\Infrastructure\ValueObject\Url\StrictUrl;
use Doctrine\Common\Collections\ArrayCollection;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Ingredient;
use Doctrine\Common\Collections\Collection;

class Recipe
{
    /**
     * @var RecipeId
     */
    private $id;

    /**
     * @var RecipeName
     */
    private $name;

    /**
     * @var Author
     */
    private $author;

    /**
     * @var string
     */
    private $image;

    /**
     * @var StrictUrl
     */
    private $url;

    /**
     * @var ArrayCollection
     */
    private $ingredients;

    public function __construct(
        RecipeId $id,
        RecipeName $name,
        Author $author
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->author = $author;
        $this->ingredients = new ArrayCollection();
    }

    public function getId(): RecipeId
    {
        return $this->id;
    }

    public function getName(): RecipeName
    {
        return $this->name;
    }

    public function setName(RecipeName $name)
    {
        $this->name = $name;
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function setAuthor(Author $author)
    {
        $this->author = $author;
    }

    public function getImage():? string
    {
        return $this->image;
    }

    public function setImage(string $image)
    {
        $this->image = $image;
    }

    public function getUrl():? StrictUrl
    {
        return $this->url;
    }

    public function setUrl(StrictUrl $url)
    {
        $this->url = $url;
    }

    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient)
    {
        $ingredient->setRecipe($this);
        $this->ingredients->add($ingredient);
    }

    public function removeIngredient(Ingredient $ingredient)
    {
        $this->removeElement($ingredient);
    }
}

<?php

namespace Tobinho\Mealtime\Domain\Recipe;

use Tobinho\Mealtime\Domain\Recipe\Recipe;
use Tobinho\Mealtime\Domain\Recipe\RecipeId;

interface RecipeRepository
{
    public function save(Recipe $recipe);

    public function delete(Recipe $recipe);

    public function all(): array;

    public function one(RecipeId $recipeId):? Recipe;
}

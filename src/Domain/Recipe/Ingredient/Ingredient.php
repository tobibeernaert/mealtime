<?php

namespace Tobinho\Mealtime\Domain\Recipe\Ingredient;

use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientName;
use Tobinho\Mealtime\Infrastructure\ValueObject\String\StringLiteral;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Amount\Amount;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientId;
use Tobinho\Mealtime\Domain\Recipe\Recipe;

class Ingredient
{
    /**
     * @var IngredientId
     */
    private $id;

    /**
     * @var IngredientName
     */
    private $name;

    /**
     * @var Amount
     */
    private $amount;

    /**
     * @var StringLiteral
     */
    private $description;

    /**
     * @var Recipe
     */
    private $recipe;

    public function __construct(IngredientId $ingredientId, IngredientName $ingredientName)
    {
        $this->id = $ingredientId;
        $this->name = $ingredientName;
    }

    public function getId(): IngredientId
    {
        return $this->id;
    }

    public function getIngredientName(): IngredientName
    {
        return $this->name;
    }

    public function setIngredientName(IngredientName $name)
    {
        $this->name = $name;
    }

    public function getAmount():? Amount
    {
        return $this->amount;
    }

    public function setAmount(Amount $amount = null)
    {
        $this->amount = $amount;
    }

    public function hasAmount(): bool
    {
        return null !== $this->amount;
    }

    public function getDescription(): StringLiteral
    {
        return $this->description;
    }

    public function setDescription(StringLiteral $description)
    {
        $this->description = $description;
    }

    public function getRecipe(): Recipe
    {
        return $this->recipe;
    }

    public function setRecipe(Recipe $recipe)
    {
        $this->recipe = $recipe;
    }

    public function __toString(): string
    {
        return sprintf(
            '%s %s%s',
            $this->amount,
            $this->name,
            (null !== $this->description ? ', '.$this->description : '')
        );
    }
}

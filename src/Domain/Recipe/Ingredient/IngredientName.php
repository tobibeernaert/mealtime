<?php

namespace Tobinho\Mealtime\Domain\Recipe\Ingredient;

use Tobinho\Mealtime\Infrastructure\ValueObject\String\NonEmptyStringLiteral;

class IngredientName extends NonEmptyStringLiteral
{
}

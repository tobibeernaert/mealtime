<?php

namespace Tobinho\Mealtime\Domain\Recipe\Ingredient;

use Tobinho\Mealtime\Infrastructure\ValueObject\Identifier\UuidIdentifier;

class IngredientId extends UuidIdentifier
{
}

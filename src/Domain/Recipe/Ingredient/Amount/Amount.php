<?php

namespace Tobinho\Mealtime\Domain\Recipe\Ingredient\Amount;

use Tobinho\Mealtime\Domain\Unit\Unit;

class Amount
{
    /**
     * @var float
     */
    private $value;

    /**
     * @var Unit
     */
    private $unit;

    public function __construct(float $value)
    {
        $this->value = $value;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function setUnit(Unit $unit)
    {
        $this->unit = $unit;
    }

    public function getUnit():? Unit
    {
        return $this->unit;
    }

    public function convert(Unit $unit)
    {
        $this->value = $this->unit->convert($this->value, $unit);
        $this->unit = $unit;
    }

    public function __toString()
    {
        return $this->unit
            ? sprintf('%g %s', $this->value, $this->unit)
            : sprintf('%g', $this->value);
    }

    public function toArray(): array
    {
        $array = ['value' => $this->getValue()];
        if ($this->unit) {
            $array['type'] = $this->getUnit()->getType();
            $array['unit'] = $this->getUnit()->getUnit();
        }

        return $array;
    }

    public function isNull(): bool
    {
        return null === $this->value;
    }
}

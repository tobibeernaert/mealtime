<?php

namespace Tobinho\Mealtime\Domain\Recipe\Ingredient;

use Tobinho\Mealtime\Domain\Recipe\Ingredient\Ingredient;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientId;

interface IngredientRepository
{
    public function save(Ingredient $ingredient);

    public function delete(Ingredient $ingredient);

    public function all(): array;

    public function one(IngredientId $ingredientId):? Ingredient;
}

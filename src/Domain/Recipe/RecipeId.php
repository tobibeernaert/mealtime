<?php

namespace Tobinho\Mealtime\Domain\Recipe;

use Tobinho\Mealtime\Infrastructure\ValueObject\Identifier\UuidIdentifier;

class RecipeId extends UuidIdentifier
{
}

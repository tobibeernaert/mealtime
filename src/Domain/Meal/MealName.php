<?php

namespace Tobinho\Mealtime\Domain\Meal;

use Tobinho\Mealtime\Infrastructure\ValueObject\String\NonEmptyStringLiteral;

class MealName extends NonEmptyStringLiteral
{
}

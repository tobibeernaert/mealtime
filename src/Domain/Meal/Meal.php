<?php

namespace Tobinho\Mealtime\Domain\Meal;

use Tobinho\Mealtime\Domain\Meal\MealType;
use Tobinho\Mealtime\Domain\Meal\MealName;
use Tobinho\Mealtime\Domain\Meal\RecipeUrl;
use Tobinho\Mealtime\Infrastructure\ValueObject\Date\Date;

class Meal
{
    /**
     * @var MealId
     */
    private $id;

    /**
     * @var Date
     */
    private $date;

    /**
     * @var MealType
     */
    private $type;

    /**
     * @var MealName
     */
    private $name;

    /**
     * @var RecipeUrl
     */
    private $recipeUrl;

    public function __construct(
        MealId $id,
        Date $date,
        MealType $type,
        MealName $name,
        RecipeUrl $recipeUrl = null
    ) {
        $this->id = $id;
        $this->date = $date;
        $this->type = $type;
        $this->name = $name;
        $this->recipeUrl = $recipeUrl;
    }

    public function getId(): MealId
    {
        return $this->id;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function setDate(Date $date)
    {
        $this->date = $date;
    }

    public function getType(): MealType
    {
        return $this->type;
    }

    public function setType(MealType $type)
    {
        $this->type = $type;
    }

    public function getName(): MealName
    {
        return $this->name;
    }

    public function setName(MealName $name)
    {
        $this->name = $name;
    }

    public function getRecipeUrl():? RecipeUrl
    {
        return $this->recipeUrl;
    }

    public function setRecipeUrl(RecipeUrl $recipeUrl)
    {
        $this->recipeUrl = $recipeUrl;
    }
}

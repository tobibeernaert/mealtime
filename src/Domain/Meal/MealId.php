<?php

namespace Tobinho\Mealtime\Domain\Meal;

use Tobinho\Mealtime\Infrastructure\ValueObject\Identifier\UuidIdentifier;

class MealId extends UuidIdentifier
{
}

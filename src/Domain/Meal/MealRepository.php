<?php

namespace Tobinho\Mealtime\Domain\Meal;

use Tobinho\Mealtime\Domain\Meal\Meal;

interface MealRepository
{
    public function save(Meal $meal);

    public function delete(Meal $meal);
}

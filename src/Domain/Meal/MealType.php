<?php

namespace Tobinho\Mealtime\Domain\Meal;

use Tobinho\Mealtime\Infrastructure\ValueObject\Enum;

class MealType extends Enum
{
    const BREAKFAST = "breakfast";
    const LUNCH = "lunch";
    const DINNER = "dinner";

    public static function breakfast(): MealType
    {
        return new static(self::BREAKFAST);
    }

    public static function lunch(): MealType
    {
        return new static(self::LUNCH);
    }

    public static function dinner(): MealType
    {
        return new static(self::DINNER);
    }
}

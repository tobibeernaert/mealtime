<?php

namespace Tobinho\Mealtime\Tests\Unit\Domain\Author\Transformer;

use PHPUnit\Framework\TestCase;
use Tobinho\Mealtime\Domain\Author\Author;
use Tobinho\Mealtime\Domain\Author\AuthorId;
use Tobinho\Mealtime\Domain\Author\AuthorName;
use Tobinho\Mealtime\Infrastructure\ValueObject\Url\StrictUrl;
use Tobinho\Mealtime\Application\Author\Transformer\AuthorTransformer;

class AuthorTransformerTest extends TestCase
{
    /**
     * @test
     */
    public function itTransformsAnAuthor()
    {
        $authorId = '1234-1234-1234-1234';

        $author = new Author(
            new AuthorId($authorId),
            new AuthorName('Yolo Swag'),
            new StrictUrl('http://www.example.com')
        );
        $result = (new AuthorTransformer())->transform($author);

        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('website', $result);

        $this->assertSame($authorId, $result['id']);
        $this->assertTrue((new AuthorName('Yolo Swag'))->equals($result['name']));
        $this->assertTrue((new StrictUrl('http://www.example.com'))->equals($result['website']));
    }
}

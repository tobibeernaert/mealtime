<?php

namespace Tobinho\Mealtime\Tests\Unit\Domain\Meal;

use PHPUnit\Framework\TestCase;
use Tobinho\Mealtime\Domain\Meal\Meal;
use Tobinho\Mealtime\Infrastructure\ValueObject\Date\Date;
use Tobinho\Mealtime\Domain\Meal\MealType;
use Tobinho\Mealtime\Domain\Meal\MealName;
use Tobinho\Mealtime\Domain\Meal\RecipeUrl;
use DateTime;

class MealTest extends TestCase
{
    /**
     * @test
     */
    public function itCanConstructAValidMealEntity()
    {
        $meal= new Meal(
            Date::fromDateTime(new DateTime("2017-01-01")),
            MealType::lunch(),
            new MealName("Test meal"),
            new RecipeUrl("http://www.example.com/recipe")
        );

        $this->assertClassHasAttribute("id", Meal::class);
        $this->assertClassHasAttribute("date", Meal::class);
        $this->assertClassHasAttribute("type", Meal::class);
        $this->assertClassHasAttribute("name", Meal::class);
        $this->assertClassHasAttribute("recipeUrl", Meal::class);

        $this->assertEquals("2017-01-01", $meal->getDate()->format('Y-m-d'));
        $this->assertEquals(new MealName("Test meal"), $meal->getName());
        $this->assertEquals(new RecipeUrl("http://www.example.com/recipe"), $meal->getRecipeUrl());
    }
}

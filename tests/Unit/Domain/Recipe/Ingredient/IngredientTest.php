<?php

namespace Tobinho\Mealtime\Tests\Unit\Domain\Recipe\Ingredient;

use PHPUnit\Framework\TestCase;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Ingredient;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientName;
use Tobinho\Mealtime\Domain\Unit\Numeric;
use Tobinho\Mealtime\Infrastructure\ValueObject\String\StringLiteral;
use Tobinho\Mealtime\Domain\Unit\Volume;
use Tobinho\Mealtime\Domain\Unit\Size;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\IngredientId;
use Tobinho\Mealtime\Domain\Recipe\Ingredient\Amount\Amount;

class IngredientTest extends TestCase
{
    /**
     * @test
     * @group ingredient
     */
    public function itCreatesAnIngredient()
    {
        $ingredient = new Ingredient(new IngredientId(), new IngredientName('Onions'));
        $ingredient->setAmount(new Amount(2));
        $ingredient->setDescription(new StringLiteral('sliced'));

        $this->assertSame(
            '2 Onions, sliced',
            (string)$ingredient
        );

        // ----

        $ingredient = new Ingredient(new IngredientId(), new IngredientName('Olive Oil'));
        $amount = new Amount(2);
        $amount->setUnit(Volume::tablespoon());
        $ingredient->setAmount($amount);

        $this->assertSame(
            '2 tbsp Olive Oil',
            (string)$ingredient
        );

        // ----

        $ingredient = new Ingredient(new IngredientId(), new IngredientName('Olive Oil'));
        $amount = new Amount(2.5);
        $amount->setUnit(Volume::tablespoon());
        $ingredient->setAmount($amount);

        $this->assertSame(
            '2.5 tbsp Olive Oil',
            (string)$ingredient
        );

        // ----

        $ingredient = new Ingredient(new IngredientId(), new IngredientName('Free-range eggs'));
        $amount = new Amount(6);
        $amount->setUnit(Size::large());
        $ingredient->setAmount($amount);

        $this->assertSame(
            '6 large Free-range eggs',
            (string)$ingredient
        );
    }
}

<?php

namespace Tobinho\Mealtime\Tests\Unit\Domain\Recipe\Transformer;

use PHPUnit\Framework\TestCase;
use Tobinho\Mealtime\Domain\Recipe\Recipe;
use Tobinho\Mealtime\Domain\Recipe\RecipeId;
use Tobinho\Mealtime\Domain\Recipe\RecipeName;
use Tobinho\Mealtime\Domain\Author\Author;
use Tobinho\Mealtime\Domain\Author\AuthorId;
use Tobinho\Mealtime\Domain\Author\AuthorName;
use Tobinho\Mealtime\Infrastructure\ValueObject\Url\StrictUrl;
use Tobinho\Mealtime\Application\Recipe\Transformer\RecipeTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;

class RecipeTransformerTest extends TestCase
{
    /**
     * @test
     */
    public function itTransformsAnRecipe()
    {
        $recipeId = '1234-1234-1234-1234';
        $authorId = '4321-4321-4321-4321';

        $recipe = new Recipe(
            new RecipeId($recipeId),
            new RecipeName('Recipe 01'),
            new Author(
                new AuthorId($authorId),
                new AuthorName('Author Name'),
                new StrictUrl('http://www.example.com/author_name')
            )
        );
        $result = (new RecipeTransformer())->transform($recipe);

        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('url', $result);

        $this->assertSame($recipeId, $result['id']);
        $this->assertTrue((new RecipeName('Recipe 01'))->equals($result['name']));
    }

    /**
     * @test
     */
    public function itIncludesAnAuthor()
    {
        $recipeId = '1234-1234-1234-1234';
        $authorId = '4321-4321-4321-4321';

        $recipe = new Recipe(
            new RecipeId($recipeId),
            new RecipeName('Recipe 01'),
            new Author(
                new AuthorId($authorId),
                new AuthorName('Author Name'),
                new StrictUrl('http://www.example.com/author_name')
            )
        );

        $recipeCollection = new Collection([$recipe], new RecipeTransformer());
        $recipeCollectionAsArray = (new Manager())->createData($recipeCollection)->toArray();

        $this->assertSame(
            '{"data":[{"id":"1234-1234-1234-1234","name":"Recipe 01","url":null,"author":{"data":{"id":"4321-4321-4321-4321","name":"Author Name","website":"http:\/\/www.example.com\/author_name"}}}]}',
            json_encode($recipeCollectionAsArray)
        );
    }
}

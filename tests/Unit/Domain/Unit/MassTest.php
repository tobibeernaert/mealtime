<?php

namespace Tobinho\Mealtime\Tests\Unit\Domain\Unit;

use PHPUnit\Framework\TestCase;
use Tobinho\Mealtime\Domain\Unit\Mass;

class MassTest extends TestCase
{
    public function provideMass()
    {
        return [
            [Mass::gram(), 1000, Mass::KILOGRAM, 1.000],
            [Mass::gram(), 1000, Mass::OUNCE, 35.274],
            [Mass::gram(), 1000, Mass::POUND, 2.205],

            [Mass::ounce(), 100, Mass::GRAM, 2834.952],
            [Mass::ounce(), 100, Mass::KILOGRAM, 2.835],
            [Mass::ounce(), 100, Mass::POUND, 6.25],

            [Mass::pound(), 1.2, Mass::GRAM, 544.311],
            [Mass::pound(), 1.2, Mass::KILOGRAM, 0.544],
            [Mass::pound(), 1.2, Mass::OUNCE, 19.2],

            [Mass::kilogram(), 0.5, Mass::GRAM, 500.0],
            [Mass::kilogram(), 0.5, Mass::OUNCE, 17.637],
            [Mass::kilogram(), 0.5, Mass::POUND, 1.102],
        ];
    }

    /**
     * @test
     * @dataProvider provideMass
     * @group mass
     */
    public function itCanConvertAMass($massUnit, $value, $newMassUnit, $newMassValue)
    {
        $newValue = $massUnit->convert($value, $newMassUnit);
        $this->assertSame($newMassValue, round($newValue, 3));
    }
}

<?php

namespace Tobinho\Mealtime\Tests\Unit\Domain\Unit;

use PHPUnit\Framework\TestCase;
use Tobinho\Mealtime\Domain\Unit\Volume;

class VolumeTest extends TestCase
{
    public function provideVolume()
    {
        return [
            [Volume::millilitre(), 1000, Volume::CENTILITRE, 100.0],
            [Volume::millilitre(), 1000, Volume::DECILITRE, 10.0],
            [Volume::millilitre(), 1000, Volume::LITRE, 1.0],
            [Volume::millilitre(), 1000, Volume::GALLON, 0.264],
            [Volume::millilitre(), 1000, Volume::QUART, 1.057],
            [Volume::millilitre(), 1000, Volume::PINT, 2.113],

            [Volume::centilitre(), 20, Volume::MILLILITRE, 200.0],
            [Volume::centilitre(), 20, Volume::DECILITRE, 2.0],
            [Volume::centilitre(), 20, Volume::LITRE, 0.2],
            [Volume::centilitre(), 20, Volume::GALLON, 0.053],
            [Volume::centilitre(), 20, Volume::QUART, 0.211],
            [Volume::centilitre(), 20, Volume::PINT, 0.423],

            [Volume::decilitre(), 15, Volume::MILLILITRE, 1500.0],
            [Volume::decilitre(), 15, Volume::CENTILITRE, 150.0],
            [Volume::decilitre(), 15, Volume::LITRE, 1.5],
            [Volume::decilitre(), 15, Volume::GALLON, 0.396],
            [Volume::decilitre(), 15, Volume::QUART, 1.585],
            [Volume::decilitre(), 15, Volume::PINT, 3.170],

            [Volume::litre(), 1.12, Volume::MILLILITRE, 1120.0],
            [Volume::litre(), 1.12, Volume::CENTILITRE, 112.0],
            [Volume::litre(), 1.12, Volume::DECILITRE, 11.2],
            [Volume::litre(), 1.12, Volume::GALLON, 0.296],
            [Volume::litre(), 1.12, Volume::QUART, 1.183],
            [Volume::litre(), 1.12, Volume::PINT, 2.367],

            [Volume::gallon(), 3, Volume::MILLILITRE, 11356.23],
            [Volume::gallon(), 3, Volume::CENTILITRE, 1135.623],
            [Volume::gallon(), 3, Volume::DECILITRE, 113.562],
            [Volume::gallon(), 3, Volume::LITRE, 11.356],
            [Volume::gallon(), 3, Volume::QUART, 12.0],
            [Volume::gallon(), 3, Volume::PINT, 24.0],

            [Volume::quart(), 5, Volume::MILLILITRE, 4731.765],
            [Volume::quart(), 5, Volume::CENTILITRE, 473.177],
            [Volume::quart(), 5, Volume::DECILITRE, 47.318],
            [Volume::quart(), 5, Volume::LITRE, 4.732],
            [Volume::quart(), 5, Volume::GALLON, 1.25],
            [Volume::quart(), 5, Volume::PINT, 10.0],

            [Volume::pint(), 1, Volume::MILLILITRE, 473.176],
            [Volume::pint(), 1, Volume::CENTILITRE, 47.318],
            [Volume::pint(), 1, Volume::DECILITRE, 4.732],
            [Volume::pint(), 1, Volume::LITRE, 0.473],
            [Volume::pint(), 1, Volume::GALLON, 0.125],
            [Volume::pint(), 1, Volume::QUART, 0.5],
        ];
    }

    /**
     * @test
     * @dataProvider provideVolume
     * @group volume
     */
    public function itCanConvertAVolume($volumeUnit, $value, $newVolumeUnit, $newVolumeValue)
    {
        $newValue = $volumeUnit->convert($value, $newVolumeUnit);
        $this->assertSame($newVolumeValue, round($newValue, 3));
    }
}

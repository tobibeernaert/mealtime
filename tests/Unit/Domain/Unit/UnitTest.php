<?php

namespace Tobinho\Mealtime\Tests\Unit\Domain\Unit;

use PHPUnit\Framework\TestCase;
use InvalidArgumentException;
use Tobinho\Mealtime\Domain\Unit\Volume;
use Tobinho\Mealtime\Domain\Unit\Mass;

class UnitTest extends TestCase
{
    /**
     * @test
     * @group unit
     * @expectedException InvalidArgumentException
     */
    public function itCanNotConvertBetweenDifferentUnits()
    {
        (Volume::litre())->convert(1, Mass::KILOGRAM);
    }
}

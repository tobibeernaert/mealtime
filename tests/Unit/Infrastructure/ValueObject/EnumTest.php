<?php

namespace Tobinho\Mealtime\Tests\Unit\Infrastructure\ValueObject;

use PHPUnit\Framework\TestCase;
use Tobinho\Mealtime\Infrastructure\ValueObject\Enum;

class EnumTest extends TestCase
{
    /**
     * @test
     * @group unit
     * @group enum
     * @covers \Tobinho\Mealtime\Infrastructure\ValueObject\Enum::__construct
     */
    public function itAllowsConstruction()
    {
        $enum = new StubEnum(StubEnum::BAR);

        $this->assertEquals('bar', (string)$enum);
    }

    /**
     * @test
     * @group unit
     * @group enum
     * @covers \Tobinho\Mealtime\Infrastructure\ValueObject\Enum::__construct
     */
    public function itThrowsExceptionOnIncorrectValue()
    {
        $this->expectException(\InvalidArgumentException::class);
        new StubEnum('baz');
    }

    /**
     * @test
     * @group unit
     * @group enum
     * @covers \Tobinho\Mealtime\Infrastructure\ValueObject\Enum::isConstantDefined
     */
    public function itReturnsTrueWhenTheConstantIsDefined()
    {
        $enum = new StubEnum(StubEnum::BAR);

        $this->assertTrue($enum->isConstantDefined('foo'));
    }

    /**
     * @test
     * @group unit
     * @group enum
     * @covers \Tobinho\Mealtime\Infrastructure\ValueObject\Enum::isConstantDefined
     */
    public function itReturnsFalseWhenTheConstantIsDefined()
    {
        $enum = new StubEnum(StubEnum::BAR);

        $this->assertFalse($enum->isConstantDefined('baz'));
    }

    /**
     * @test
     * @group unit
     * @group enum
     * @covers \Tobinho\Mealtime\Infrastructure\ValueObject\Enum::getName
     */
    public function itReturnsTheEnumName()
    {
        $enum = new StubEnum(StubEnum::BAR);

        $this->assertSame('BAR', $enum->getName());
    }

    /**
     * @test
     * @group unit
     * @group enum
     * @covers \Tobinho\Mealtime\Infrastructure\ValueObject\Enum::__toString
     */
    public function itCastsToString()
    {
        $enum = new StubEnum(StubEnum::BAR);
        $this->assertSame('bar', (string)$enum);
    }

    /**
     * @test
     * @group unit
     * @group enum
     * @covers \Tobinho\Mealtime\Infrastructure\ValueObject\Enum::__construct
     */
    public function itCannotInitAnEmptyEnum()
    {
        $this->expectException(\InvalidArgumentException::class);
        new EmptyEnum('baz');
    }

    /**
     * @test
     * @group unit
     * @group enum
     * @covers \Tobinho\Mealtime\Infrastructure\ValueObject\Enum::__construct
     */
    public function itCannotIntAnEnumWithPrivateConstants()
    {
        $this->expectException(\InvalidArgumentException::class);
        new StubEnum('private');
    }
}

class StubEnum extends Enum
{
    const FOO = 'foo';
    const BAR = 'bar';
    private const PRIV = 'private';
}

class EmptyEnum extends enum
{
}

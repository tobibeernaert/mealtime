<?php

namespace Tobinho\Mealtime\Tests\Unit\Infrastructure\ValueObject\Url;

use PHPUnit\Framework\TestCase;
use Tobinho\Mealtime\Infrastructure\ValueObject\Url\Url;
use InvalidArgumentException;

class UrlTest extends TestCase
{
    public function provideValidUrl()
    {
        return [
            ["http://www.example.com"],
            ["https://www.example.com"],
            ['ftp://ftp.is.co.za.example.org/rfc/rfc1808.txt'],
            ['gopher://spinaltap.micro.umn.example.edu/00/Weather/California/Los%20Angeles'],
            ['ldap://[2001:db8::7]/c=GB?objectClass?one'],
            ['mailto:John.Doe@example.com'],
            ['mailto:mduerst@ifi.unizh.example.gov'],
            ['news:comp.infosystems.www.servers.unix'],
            ['news:comp.infosystems.www.servers.unix'],
            ['telnet://192.0.2.16:80/'],
            ['telnet://melvyl.ucop.example.edu/'],
        ];
    }

    /**
     * @test
     * @dataProvider provideValidUrl
     */
    public function itWillValidateAnURL($url)
    {
        $strictUrl = new Url($url);
        $this->assertTrue(true);
    }

    public function provideInvalidUrl()
    {
        return [
            ["www.example.com"],
            ["example.com"],
            ["example."],
        ];
    }

    /**
     * @test
     * @dataProvider provideInvalidUrl
     */
    public function itFailsToValidateAnURL($url)
    {
        $this->expectException(InvalidArgumentException::class);
        $strictUrl = new Url($url);
    }
}

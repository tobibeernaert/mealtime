<?php

namespace Tobinho\Mealtime\Tests\Unit\Infrastructure\ValueObject\String;

use PHPUnit\Framework\TestCase;
use Tobinho\Mealtime\Infrastructure\ValueObject\String\StringLiteral;

class StringLiteralTest extends TestCase
{
    public function provideString()
    {
        return [
            ["test", "test"],
            ["", ""],
            [1, "1"],
            [" ", " "],
        ];
    }

    /**
     * @test
     * @dataProvider provideString
     */
    public function itConstructsALiteralString($string, $expectedResult)
    {
        $literalString = new StringLiteral($string);
        $this->assertSame($expectedResult, $literalString->__toString());
    }

    /**
     * @test
     * @dataProvider provideString
     */
    public function itConstructsALiteralStringFromAString($string, $expectedResult)
    {
        $literalString = StringLiteral::fromString($string);
        $this->assertSame($expectedResult, $literalString->__toString());
    }

    /**
     * @test
     */
    public function itMapsAnArrayOfStringToStringLiterals()
    {
        $literalStrings = StringLiteral::map([
            "string",
            "",
            " ",
            "-&23^",
        ]);
        foreach ($literalStrings as $literalString) {
            $this->assertInstanceOf(StringLiteral::class, $literalString);
        }
    }

    /**
     * @test
     */
    public function itCanSerializeAStringLiteral()
    {
        $string = new StringLiteral("string");

        $this->assertSame(["value" => "string"], $string->serialize());
    }
}
